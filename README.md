# Useful data for Open Elections

You don't need this data to run Open Elections so they're not part of the [main repository](https://gitlab.com/open-elections/open-elections.gitlab.io), but they're convenient to have.

- [List of countries with corresponding iso codes](https://gitlab.com/open-elections/useful-data/-/blob/master/countries.csv)
- [Flag icons of tons of countries](https://gitlab.com/open-elections/useful-data/-/tree/master/flags) courtesy of [famfamfam](http://www.famfamfam.com/lab/icons/flags/)